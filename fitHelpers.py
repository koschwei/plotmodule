import ROOT
import ctypes
import logging

def getCHiSquare(h1, h2, option="UW"):
    """
    Wrapper function for getting the chi2 from two histograms. The default configuration expects h1 to be the 
    UNWEIGHTED data histograms and h2 to be the WEIGHTED histogram from simulation

    Description see: https://root.cern.ch/doc/master/classTH1.html#a6c281eebc0c0a848e7a0d620425090a5

    Function checks if the fit returns a problematic exit code from to little events in certain bins and will remove
    these bins from the calculation.

    Args:
      h1 (ROOT.TH1) : Input histogram that is compaed agains
      h2 (ROOT.TH1) : Input histogram that is compared
      option (str) : Option as defined ROOT::TH1::Chi2Test

    Returns:
      pVal (float) : p-Value of the chi2 test
      chi2Val/ndof (float) : Chi2 value of the chi2test devided by the degrees of freedom
      ndof (int) : Degrees of freedom
      iGood (bool) : Flag if a modified histogram was used for the test
    """
    # if option != "UW":
    #     raise NotImplementedError("Only tested for UW not %s. Remove this error error if you don't care"%option)
    
    validResult = False
    fitResults = None

    
    chi2Val = ROOT.Double(-1.0)
    ndof = ctypes.c_int(-1)
    iGood = ctypes.c_int(0)

    pVal = h1.Chi2TestX(h2, chi2Val, ndof, iGood, option)

    if iGood.value == 0:
        logging.debug("All good on first try")
        fitResults = (pVal, chi2Val/float(ndof.value), ndof.value, False)
    else:
        h1Cleaned = h1.Clone("h1_cleaned")
        h2Cleaned = h2.Clone("h2_cleaned")
        if iGood.value == 1 or iGood.value == 3:
            for i in range(h1.GetNbinsX()):
                if h1Cleaned.GetBinContent(i+1) < 1:
                    logging.debug("Set binconten of h1 - %s to 0", h1Cleaned.GetName())
                    h1Cleaned.SetBinContent(i+1, 0)
                    h1Cleaned.SetBinError(i+1, 0)
                    h2Cleaned.SetBinContent(i+1, 0)
                    h2Cleaned.SetBinError(i+1, 0)
        if iGood.value == 2 or iGood.value == 3:
            for i in range(h2.GetNbinsX()):
                if h2Cleaned.GetBinContent(i+1) < 1:
                    logging.debug("Set binconten of h2 - %s to 0", i+1)
                    h2Cleaned.SetBinContent(i+1, 0)
                    h2Cleaned.SetBinError(i+1, 0)
                    h1Cleaned.SetBinContent(i+1, 0)
                    h1Cleaned.SetBinError(i+1, 0)
        pVal = h1Cleaned.Chi2TestX(h2Cleaned, chi2Val, ndof, iGood, option)
        fitResults = (pVal, chi2Val/float(ndof.value), ndof.value, True)
        del h1Cleaned, h2Cleaned
    logging.info("pVal = %s, chi2 = %s, ndof = %s, iGood = %s", pVal, chi2Val, ndof.value, iGood.value)
    
    return fitResults


