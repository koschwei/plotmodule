"""
Implementation for plotting ROC curves
"""
import logging
from copy import deepcopy

import ROOT

import PlotHelpers

class ROC(object):
    """
    Class for ROC plots
    """
    def __init__(self, name, width=600, height=540, rocType="sEff-bEff"):
        """
        Initialization for ROC plot

        Arguments:
        ----------
        name : str 
            Will be used in the canvas name. Should be unique
        width, height : int
            Canvas width and height
        rocType : str
            ROCs can be plotted in different combinations
            Currently supported:
            sEffbEff (x: Signal Efficiency, y: Background Efficiency)

        """
        logging.debug("Initializing class")
        self.canvas = ROOT.TCanvas("canvas_"+str(name),"canvas_"+str(name),width, height)
        self.canvas.SetTicks(1,1)
        self.canvas.SetFillStyle(0)
        self.canvas.SetFillColor(0)

        self.canvas.SetTopMargin(0.068)
        self.canvas.SetRightMargin(0.04)
        self.canvas.SetLeftMargin(0.12)
        self.canvas.SetBottomMargin(0.12)

        self.hSignal = []
        self.hBackground = []
        self.nHistoPairs = 0
        
        self.labels = None
        
        self.validROCS = ["sEff-bEff","sEff-bRej"]
        self.ROCdefault = "sEff-bEff"
        if rocType not in self.validROCS:
            logging.error("Invalid ROC type was set. Will return to defaults %s", self.ROCdefault)
            self.rocType = self.ROCdefault
        else:
            self.rocType = rocType

        if self.rocType == "sEff-bRej":
            self.legendSize = (0.15,0.15,0.55,0.4)
        else:
            self.legendSize = (0.15,0.55,0.55,0.8)
            
        self.color = []
        self.setVarAsTitle = False
        self.canvasTitle = None
        self.invert = False
        self.CMSscale = (1.0, 1.0)
        self.moveCMS = 0
        self.thisCMSLabel = "Preliminary"
        
    def passHistos(self, signal, background):
        """
        Way to pass the necesary histograms to the class
        """
        logging.debug("Setting signal and background histos")
        self.hSignal.append(signal)
        logging.debug("Added %s",signal)
        self.hBackground.append(background)
        logging.debug("Added %s",background)
        self.nHistoPairs += 1
        self.color.append(background.GetLineColor())
        
    def addCMSLabel(self):
        cms = ROOT.TLatex( 0.1, 0.8 , '#scale['+str((1)+0.2)+']{#bf{CMS}} #scale['+str((1))+']{#it{simulation}}')
        cms.SetTextFont(42)
        cms.SetTextSize(0.045)
        cms.SetNDC()

        self.labels.append(cms)

    def addLabel(self, text, xPos, yPos, align = "Right", scaleText = 1):
        l = ROOT.TLatex(xPos, yPos, text);
        l.SetNDC();
        if align == "Right":
            l.SetTextAlign(32);
        elif align == "Left":
            l.SetTextAlign(12);
        l.SetTextFont(42);
        l.SetTextSizePixels(10*scaleText);
        if self.labels is None:
            self.labels = []
        self.labels.append(l)
        
    def _calcValues_ (self, histo, what):
        """
        Function for calculating the values for the ROC depending on the type
        Arguments:
        ----------
        histo : ROOT.TH1F
            Histogram to calculate the ROC for
        what : str
            Calcualtion type. Currently supported:
            - Efficiency
        """
        logging.debug("Calculating ROC for %s",histo)
        nBinsX = histo.GetNbinsX()
        nTot = histo.Integral()
        if what == "Efficiency":
            vals = [1]
        elif  what == "Rejection":
            vals = [0]
        nSum = 0
        for iBin in range(nBinsX + 1):
            if what == "Efficiency":
                nSum += histo.GetBinContent(iBin)
                if nTot != 0:
                    thisVal = 1 - (nSum / nTot)
                else:
                    thisVal = 0
                vals.append(thisVal)
                logging.debug("Adding value %s",thisVal)
            elif what == "Rejection":
                nSum += histo.GetBinContent(iBin)
                if nTot != 0:
                    thisVal = 1 - (nSum / nTot)
                else:
                    thisVal = 0
                vals.append(1 - thisVal)
                logging.debug("Adding value %s",1 - thisVal)
            else:
                raise NotImplementedError
        return vals

    def getTitleAndCalc(self):
        if self.rocType == "sEff-bEff":
            if not self.invert:
                xTitle = "Signal Efficiency"
                yTitle = "Background Efficiency"
                xCalc = "Efficiency"
                yCalc = "Efficiency"
            else:
                xTitle = "Signal Efficiency"
                yTitle = "Background Efficiency"
                xCalc = "Rejection"
                yCalc = "Rejection"
        elif self.rocType == "sEff-bRej":
            if not self.invert:
                xTitle = "Signal Efficiency"
                yTitle = "Background Rejection"
                xCalc = "Efficiency"
                yCalc = "Rejection"
            else:
                xTitle = "Signal Efficiency"
                yTitle = "Background Rejection"
                xCalc = "Rejection"
                yCalc = "Efficiency"

        else:
            raise NotImplementedError
        return xTitle, yTitle, xCalc, yCalc
    
    def drawROC(self, legendText, invert = False, drawCMS = False):
        """
        Function that will loop over all passed histogram pairs, calculate the ROC, AUC and make the canvas
        
        Args:
        -----
        legendText (list) : List of elements for the legend --> Needs to be of same lenths as passed histogram pairs
        invert (bool) : This will change the calculation mehtod. If False it will be assumed, that the Signal
                        is towards higher values that the background. Set to True if the oposite is the case.
        """
        self.invert = invert
        xTitle, yTitle, xCalc, yCalc = self.getTitleAndCalc()
        
        if self.nHistoPairs == 0:
            raise RuntimeError("NoHistograms set")

        assert isinstance(legendText, list)
        assert len(legendText) == self.nHistoPairs
        
        logging.info("%s pairs set", self.nHistoPairs)
        logging.debug("Will Draw ROCs for following combinations:")
        for ipair in range(self.nHistoPairs):
            logging.debug("Sig: %s", self.hSignal[ipair])
            logging.debug("Bkg: %s", self.hBackground[ipair])
            logging.debug("-----------------------")
            
        leg = ROOT.TLegend(self.legendSize[0], self.legendSize[1], self.legendSize[2], self.legendSize[3])
        leg.SetFillStyle(0)
        leg.SetBorderSize(0)
        leg.SetTextSize(0.03)

        self.canvas.cd()
        graphs = []
        aucs = []
        for ipair in range(self.nHistoPairs):
            logging.info("Processing pair %s", ipair)
            nBinsX = self.hSignal[ipair].GetNbinsX()
    
            if nBinsX != self.hBackground[ipair].GetNbinsX():
                raise RuntimeError("Signal and background histos have different number of bins")
            logging.debug("Histograms have %s bins", nBinsX)
            
            vals = [(1,1)]
            g = ROOT.TGraph(nBinsX)
            calcSig = self._calcValues_(self.hSignal[ipair], xCalc)
            calcBkg = self._calcValues_(self.hBackground[ipair], yCalc)
            vals = zip(calcSig, calcBkg)

            for ival in range(len(vals)):
                g.SetPoint(ival, vals[ival][0], vals[ival][1])


            # TGraph::Integral calcualtes the area spanned by shape, where the last and the first point
            #  are connected. This means that a additionl point in the lower right corener has to be added
            #  so the correct value is calculated
            g4Int = ROOT.TGraph(nBinsX+1)
            for ival in range(len(vals)):
                g4Int.SetPoint(ival, vals[ival][0], vals[ival][1])
            g4Int.SetPoint(ival+1, 1, 0)
            auc = 0.0
            auc = g4Int.Integral()

            if auc > 0.5:
                logging.warning("Auto inversion due to ROC > 0.5 in pair %s", ipair)
                vals = [(1,1)]
                g = ROOT.TGraph(nBinsX)
                calcSig = self._calcValues_(self.hBackground[ipair], xCalc)
                calcBkg = self._calcValues_(self.hSignal[ipair], yCalc)
                vals = zip(calcSig, calcBkg)
                
                for ival in range(len(vals)):
                    g.SetPoint(ival, vals[ival][0], vals[ival][1])
                    
                    
                # TGraph::Integral calcualtes the area spanned by shape, where the last and the first point
                #  are connected. This means that a additionl point in the lower right corener has to be added
                #  so the correct value is calculated
                g4Int = ROOT.TGraph(nBinsX+1)
                for ival in range(len(vals)):
                    g4Int.SetPoint(ival, vals[ival][0], vals[ival][1])
                g4Int.SetPoint(ival+1, 1, 0)
                auc = 0.0
                auc = g4Int.Integral()

            aucs.append(auc)

            
            #h = g.GetHistogram()
            if self.setVarAsTitle:
                logging.debug("Using variable as Title")
                g.SetTitle(self.hSignal[ipair].GetXaxis().GetTitle())
                self.canvas.SetTopMargin(0.08)
            elif self.canvasTitle is not None:
                logging.debug("Using canvasTitle member as Title")
                g.SetTitle(self.canvasTitle)
                ROOT.gStyle.SetTitleH(0.035)
                self.canvas.SetTopMargin(0.08)
            else:
                g.SetTitle("")
            g.GetXaxis().SetRangeUser(0,1)
            g.GetYaxis().SetRangeUser(0,1)
            g.GetXaxis().SetTitle(xTitle)
            g.GetXaxis().SetTitleSize( g.GetXaxis().GetTitleSize() * 1.2)
            g.GetYaxis().SetTitleSize( g.GetYaxis().GetTitleSize() * 1.2)
            g.GetYaxis().SetTitle(yTitle)
            g.SetLineColor(self.color[ipair])
            g.SetMarkerColor(self.color[ipair])
            g.SetLineWidth(2)
            graphs.append(deepcopy(g))
        


        for ipair in range(self.nHistoPairs):
            logging.debug("Drawing ROC for pair %s", ipair)
            leg.AddEntry(graphs[ipair], "{0} | AUC: {1:0.3f}".format(legendText[ipair], aucs[ipair]), "L")
            if ipair == 0:
                #print "x--------------",graphs[ipair].GetXaxis().GetNdivisions()
                #print "y--------------",graphs[ipair].GetYaxis().GetNdivisions()
                graphs[ipair].Draw("")
            else:
                graphs[ipair].Draw("same")

        
        CMSSize, ExtraSize = 0.6, 0.75
        if drawCMS:
            scalelumi, scaleCMS = self.CMSscale
            logging.debug("Using scale for CMS labels: %s",self.CMSscale)
            CMSSize, ExtraSize = PlotHelpers.drawCMSLabel(self.thisCMSLabel, self.canvas.GetTopMargin(),  self.canvas.GetRightMargin(), scalelumi, scaleCMS, self.moveCMS)

        if self.labels is not None:
            for label in self.labels:
                if drawCMS:
                    label.SetTextSize(ExtraSize);
                label.Draw("SAME")
        if drawCMS:
            leg.SetTextSize(ExtraSize)
        leg.Draw("SAME")
        self.canvas.Update()
        #print self.canvas.GetTopMargin()
        #raw_input("")
        return deepcopy(self.canvas)
        
    def getCutTable(self, whichPair = 0):
        xTitle, yTitle, xCalc, yCalc = self.getTitleAndCalc()
        calcSig = self._calcValues_(self.hSignal[whichPair], xCalc)
        calcBkg = self._calcValues_(self.hBackground[whichPair], yCalc)

        table = []
        
        
        for iBin in range(len(calcSig)):
            table.append((self.hSignal[whichPair].GetBinLowEdge(iBin+1), calcSig[iBin], calcBkg[iBin]))
            #print self.hSignal[whichPair].GetBinLowEdge(iBin+1),calcSig[iBin], calcBkg[iBin]
        return table

    @staticmethod
    def writeTableOutput(listOfTables, headers, title, outputname, folder = ".", outFormat = "Markdown"):
        logging.info("Start wrining cut out tables")
        if outFormat not in ["Markdown"]:
            raise NotImplementedError

        logging.debug("Output mode set to %s", outFormat)
        assert len(headers) == len(listOfTables)
        
        with open(outputname+".txt", 'w') as f:
            for itable, table in enumerate(listOfTables):
                logging.debug("Writing table %s", itable)
                if itable != 0:
                    f.write("\n")
                else:
                    f.write("# {0} \n\n".format(title))
                f.write("## Table {0} \n\n".format(headers[itable]))
                if outFormat == "Markdown":
                    f.write("| cut | Signal Eff | Background Eff |\n")
                    f.write("| --- | ---------- | -------------- |\n")
                for bin_ in range(len(table)):
                    if outFormat == "Markdown":
                        f.write("| {0} | {1:.4f} | {2:.4f} |\n".format(table[bin_][0], table[bin_][1], table[bin_][2]))
        logging.info("Closed file")
        
