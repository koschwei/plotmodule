"""
Module for making easy and fast ratio plots

Usage: 
1.) Initialize the RatioPlot class with a name (so the canvas has a unique name)
2.) Add the list of histograms that you want in the plot with the addHistos method.
    The fist will be used as the reference histogram (and the line in the ratio)
3.) Run the drawPlot method and pass a list of legend labels in the order of the
    histograms passed with the addHistos method. Passing a xTitle is optional. Otherwise
    the one of the leading histogram is used.
"""
import logging
from copy import deepcopy

import ROOT

import PlotHelpers

class RatioPlot(object):
    """
    Class for making nice and fast ratio plots
    """
    def __init__(self, name, width=600, height=540):
        logging.debug("Initializing class")
        self.canvas = ROOT.TCanvas("canvas_"+str(name),"canvas_"+str(name),width, height)
        self.canvas.Divide(1,2)
        self.canvas.cd(1).SetPad(0.,0.3-0.02,1.0,0.975)
        self.canvas.cd(2).SetPad(0.,0.0,1.0,0.3*(1-0.02))
        self.canvas.cd(1).SetBottomMargin(0.02)
        self.canvas.cd(2).SetTopMargin(0.00)
        self.canvas.cd(2).SetTicks(1,1)
        self.canvas.cd(1).SetTicks(1,1)
        self.canvas.cd(2).SetFillStyle(0)
        self.canvas.cd(1).SetFillStyle(0)
        self.canvas.cd(2).SetFillColor(0)
        self.canvas.cd(2).SetFrameFillStyle(4000)
        self.canvas.cd(1).SetTopMargin(0.068)
        self.canvas.cd(1).SetRightMargin(0.06)
        self.canvas.cd(1).SetLeftMargin(0.12)
        self.canvas.cd(2).SetRightMargin(0.06)
        self.canvas.cd(2).SetLeftMargin(0.12)
        self.canvas.cd(2).SetBottomMargin(0.3)


        # self.canvas.cd(1).SetTopMargin(0.01)
        # self.canvas.cd(1).SetRightMargin(0.04)
        # self.canvas.cd(1).SetLeftMargin(0.11)
        # self.canvas.cd(2).SetRightMargin(0.04)
        # self.canvas.cd(2).SetLeftMargin(0.11)
        # self.canvas.cd(2).SetBottomMargin(0.3)

        self.histos = None
        self.ratioRange = (0.2,1.8)
        self.ratioText = "#frac{Data}{MC}"
        self.legendSize = (0.7,0.6,0.9,0.8)
        self.legColumns = 1
        self.scaleLegenText = 1
        self.labels = None
        self.yTitle = "Events"
        self.labelScaleX = 1.2
        self.defaultColors = [ROOT.kBlue, ROOT.kRed, ROOT.kGreen-2, ROOT.kOrange, ROOT.kCyan]
        self.useDefaultColors = False

        self.invertDrawOrder = False 
        self.ratioLineWidth = 1
        self.drawRatioLineErrors = False
        self.ratioRelUncertaintySelf = False

        self.errorbandUncertaintyLabel = "Uncertainty"
        
        self.yTitleOffset = 0.78
        self.xTitleOffsetScale = 1 
        self.yTitleOffsetRatio = 0.4
        self.invertRatio = False

        self.CMSscale = (1.0, 1.0)
        self.moveCMS = 0
        self.moveCMSTop = 0
        self.CMSLeft = False
        self.CMSOneLine = False
        self.thisCMSLabel = "Preliminary"
        self.thisLumi = "41.5"
        
        self.addChi2 = False
        self.addpVal = False

        self.addKSTest = False
        self.KSxStart = 0.5
        self.KSyStart = 0.52
        
        self._ROOTObjects = None
        self._ROOTObjectsSet = False

        self.addIntegral = False

        self.drawHorisontalLine = False
        self.HorisontalLineAt = 1

        self.ratioReference = None
        self.invertRatioOrder = False

        self.customRange = None
        self.doAutoRange = False

        self.drawAddHistosBackground = False

        self.histos_normalized = False
        
    def passHistos(self, ListOfHistos, normalize = False, appendHisto = False):
        if not appendHisto:
            self.histos = ListOfHistos
        else:
            if not isinstance(self.histos, list):
                self.histos =[ListOfHistos]
            else:
                self.histos.append(ListOfHistos)
        if normalize:
            self.histos_normalized = True
            self.yTitle = "Normalized Units"
            for h in self.histos:
                try:
                    1/h.Integral()
                except ZeroDivisionError:
                    logging.error("Zero Division in scaling")
                else:
                    h.Scale(1/h.Integral())

    def addLabel(self, labelText, xStart = 0.5, yStart = 0.92, scaleText=1):
        thisLabel = ROOT.TLatex(xStart , yStart , labelText)
        thisLabel.SetTextFont(42)
        thisLabel.SetTextSize(0.045*scaleText)
        thisLabel.SetNDC()
        if self.labels is None:
            self.labels = []
        self.labels.append(thisLabel)
        return True

    def getMainRangeAuto(self):
        """ Get the maximum and minimum value from all histograms """
        maxVal, minVal = 0, 0
        for histo in self.histos:
            pass

        return maxVal, minVal

        
    def drawAddHistos(self, addHisto):
        if not isinstance(addHisto, list):
            logging.error("addPlot variables is required to be of type list. Skipping add Plots")
        else:
            logging.debug("Additional histograms: %s",addHisto)
            for aHisto in addHisto:
                if isinstance(aHisto, tuple):
                    h, drawStyle = aHisto
                    logging.debug("Drawing additional object %s with style %s", h, drawStyle)
                    h.Draw(drawStyle+"same")
                else:
                    aHisto.Draw("histoesame")
    
    def drawPlot(self, legendText, xTitle = None, isDataMCwStack = False, stacksum = None, addHisto = None, plotTitle = None, noErr = False,errorband = None, drawCMS =False, histolegend = None, logscale = False, drawPulls = False, saveCanvas = False, floatingMax = 1.15, groupRatios = False, noErrRatio=False ):
        """
        Args:
            legendText
            xTitle (str) : Set the title of the x-axis if not set. If None, the title of the passed histo will be used
            isDataMCwStack (bool) : Set to True for Data/MC plots with THStacks
            stacksum (ROOT.TH1) :  Will be used to calculate the ratio of the data to the MC (stack).
            addHisto (list(ROOT.TH1)) : 
            plotTitle (str) : 
            noErr (bool) : 
            errorband (ROOT.TH1) : 
            drawCMS (bool) : 
            histolegend (list(tuple)) : 
            logscale (bool) : 
            drawPulls (bool) : 
            saveCanvas (bool) : 
            floatingMax (float) : 
            groupRatios (bool) : Setting this option to true will results in a ratioplot where the 1st, 3rd... histogram
                                 will be used as reference histogram for the 2nd, 4th... histogram
            noErrRatio (bool) : Deactivates the error bars in the ratio. Errors will also be removed when passing only noErr

        """
        maximimus = []
        minimums = []
        for h in self.histos:
            maximimus.append(h.GetMaximum())


        #logging.warning(maximimus)
        leg = ROOT.TLegend(self.legendSize[0], self.legendSize[1], self.legendSize[2], self.legendSize[3])
        logging.debug("Setting NColumns to %s",self.legColumns)
        leg.SetNColumns(self.legColumns)
        leg.SetFillStyle(0)
        leg.SetBorderSize(0)
        leg.SetTextSize(0.05*self.scaleLegenText)

        if drawPulls:
            self.ratioText = "Pulls"
        
        if self.useDefaultColors:
            if len(self.defaultColors) < len(self.histos):
                raise RuntimeError("More histograms set than default color. Please disable useDefaultColors and set colors in your script")
            for ihisto, histo in enumerate(self.histos):
                histo.SetLineColor(self.defaultColors[ihisto])
                histo.SetFillStyle(0)
        ################################################
        # Making main plot
        self.canvas.cd(1)
        if logscale and isDataMCwStack:
            self.histos[1].Draw()
            self.histos[1].SetMinimum(1)
            self.canvas.cd(1).SetLogy()

        if logscale and not isDataMCwStack:
            self.canvas.cd(1).SetLogy()
            
        loopHistos = self.histos
        if self.invertDrawOrder:
            loopHistos = self.histos[::-1]
            
        for ihisto, histo in enumerate(loopHistos):
            logging.debug("Drawing %s", histo)
            if ihisto == 0:
                
                if isDataMCwStack:
                    initDrawOption = "P"
                else:
                    if noErr:
                        initDrawOption = "histo"
                    else:
                        initDrawOption = "histoe"
                histo.Draw(initDrawOption)
                if self.drawAddHistosBackground and addHisto is not None:
                    self.drawAddHistos(addHisto)
                    histo.Draw(initDrawOption+"same")
                    
                if logscale:
                    histo.SetMaximum(max(maximimus)*1000)
                else:
                    histo.SetMaximum(max(maximimus)*floatingMax)
                if logscale:
                    histo.SetMinimum(2)
                else:
                    histo.SetMinimum(0)
                if plotTitle is None:
                    histo.SetTitle("")
                else:
                    histo.SetTitle(plotTitle)
                    self.canvas.cd(1).SetTopMargin(0.1)

                if self.customRange is not None or self.doAutoRange:
                    if self.doAutoRange:
                        cRangeMax, cRangeMin = self.getMainRangeAuto()
                    else:
                        if not isinstance(self.customRange, tuple):
                            raise RuntimeError("Pass custom range as tuple (max, min)")
                        cRangeMax, cRangeMin = self.customRange

                    histo.SetMaximum(cRangeMin)
                    histo.SetMinimum(cRangeMax)
                      
                histo.GetYaxis().SetTitle(self.yTitle)
                histo.GetXaxis().SetTitleSize(0)
                histo.GetXaxis().SetLabelSize(0)
                histo.GetYaxis().SetTitleOffset(self.yTitleOffset)
                histo.GetYaxis().SetTitleSize(0.06)
                if self.drawHorisontalLine:
                    line = ROOT.TF1("vline", str(self.HorisontalLineAt) ,-9000.0,9000.0)
                    line.SetLineColor(12) #Grey
                    line.SetLineWidth(1)
                    line.SetLineStyle(2) #Dashed
                    line.Draw("same")
            else:
                add = ""
                #if isDataMCwStack:
                #    add = "axis"
                if noErr:
                    histo.Draw("histosame"+add)
                else:
                    histo.Draw("histoesame"+add)
                    
            if legendText is not None:
                if legendText[ihisto] is not None:
                    leg.AddEntry(histo, legendText[ihisto], "PLE")
                    logging.debug("Adding %s to legend for %s",legendText[ihisto], histo)
        if addHisto is not None and not self.drawAddHistosBackground:
            self.drawAddHistos(addHisto)
        if isDataMCwStack:
            if self.addIntegral:
                tmpText = "Data ({0})".format(int(round(self.histos[0].Integral())))
            else:
                tmpText = "Data"
            leg.AddEntry(self.histos[0], tmpText, "PLE")

        if histolegend is not None:
            for legendTuple in histolegend:
                if len(legendTuple) == 2:
                    histo, text = legendTuple
                    style = "F"
                if len(legendTuple) == 3:
                    histo, text, style = legendTuple
                leg.AddEntry(histo, text, style)
        chi2 = 0
        pval = 0
        if isDataMCwStack and stacksum is not None and (self.addChi2 or self.addpVal):
            from scipy import stats
            for b in range(self.histos[0].GetNbinsX()):
                nbkg = stacksum.GetBinContent(b+1)
                ebkg = stacksum.GetBinError(b+1)
                ndata = self.histos[0].GetBinContent(b+1)
                edata = self.histos[0].GetBinError(b+1)
                #rootChi2 = hqcd_check.Chi2Test(hqcdSR,"CHI2/NDF" ,residuals)

                r = ndata / nbkg if nbkg>0 else 0
                rerr = edata / nbkg if nbkg>0 else 0
                p = (ndata-nbkg)/(edata**2+ebkg**2)**0.5 if (edata+ebkg)>0 else 0
                perr = 1.0
                chi2 += p*p

            pval = 1.0 - stats.chi2.cdf(chi2, self.histos[0].GetNbinsX()-1)
            chi2val = chi2 / (1.0*(self.histos[0].GetNbinsX()-1))
            
        if self.addChi2:
            leg.AddEntry(None, "#chi^{2}/dof = "+"{0:0.1f}".format(chi2val), "")
        if self.addpVal:
            leg.AddEntry(None, "p-value = {0:0.3f}".format(pval), "")
            
        if self.addKSTest:

            if len(self.histos) > 2:
                raise NotImplementedError
            
            KSLabelTest = ""
            for h in self.histos[1::]:
                KSTest = self.histos[0].KolmogorovTest(h)
                KSLabelTest += " KS: {0:1.4f}".format(KSTest)
                self.addLabel(KSLabelTest, self.KSxStart, self.KSyStart)
            
        if isDataMCwStack:
            self.histos[0].Draw("Psame")
            self.histos[0].Draw("sameaxis")

        if self.labels is not None:
            for label in self.labels:
                label.Draw("same")
        if errorband is not None:
            # for ibin in range(errorband.GetNbinsX()+2):
            #     print errorband.GetBinContent(ibin), errorband.GetBinError(ibin)

            if self.histos_normalized:
                logging.debug("Normalizing error band")
                try:
                    1/errorband.Integral()
                except ZeroDivisionError:
                    logging.error("Zero Division in scaling")
                else:
                    errorband.Scale(1/errorband.Integral())
                
            logging.info("Drawing errorband")
            leg.AddEntry(errorband, self.errorbandUncertaintyLabel, "F")
            errorband.Draw("E2same")

        leg.Draw("same")
        ################################################
        # Making ratio plot
        self.canvas.cd(2)
        if stacksum is not None:
            ratios = self._makeAllRatios(stacksum, drawPulls)
        else:
            ratios = self._makeAllRatios(drawPulls = drawPulls, groupRatios = groupRatios, selfRatio=self.ratioRelUncertaintySelf)
        if not groupRatios:
            if self.ratioReference is None:
                assert len(ratios) == len(self.histos)
        else:
            # groupRatios will result in half of the ratios +1 --> We still habe line but then overy other is missing
            assert len(ratios) == (len(self.histos)/2)+1
            logging.info("Will set ratio line to black. Keep this in mind when choosing you colors")
            ratios[0].SetLineColor(ROOT.kBlack)
        for iratio, ratio in enumerate(ratios):
            if iratio == 0:
                ratio.SetTitle("")
                ratio.GetXaxis().SetTitleOffset(0.8*self.xTitleOffsetScale)
                ratio.GetXaxis().SetTitleSize(0.125 )
                ratio.GetXaxis().SetLabelSize(ratio.GetYaxis().GetLabelSize()*self.labelScaleX)
                ratio.GetYaxis().SetTitleOffset(self.yTitleOffsetRatio)
                ratio.GetYaxis().SetTitleSize(0.1)
                ratio.GetYaxis().CenterTitle()
                if xTitle is not None:
                    ratio.GetXaxis().SetTitle(xTitle)
                ratio.GetYaxis().SetTitle(self.ratioText)

                if errorband is not None and stacksum is not None:
                    ratioErrorband = _getRatioErrorBand(stacksum, ratio)
                    if not drawPulls:
                        ratioErrorband.Draw("E2")
                    ratio.Draw("histoesame")
                        

                elif errorband is not None and stacksum is None:
                    ratioErrorband = _getRatioErrorBand(self.histos[0], ratio)
                    if not drawPulls:
                        ratioErrorband.Draw("E2")
                    ratio.Draw("histoesame")
                else:
                    if self.drawRatioLineErrors:
                        ratio.Draw("histoe")
                    else:
                        ratio.Draw("histo")
            else:
                if (noErr or noErrRatio) and not isDataMCwStack:
                    for i in range(ratio.GetN()):
                        ratio.SetPointEYlow(i,0)
                        ratio.SetPointEYhigh(i,0)
                    ratio.Draw("sameP")
                else:
                    ratio.Draw("sameP")
                    #ROOT.gStyle.SetErrorX(1)

        if drawCMS:
            scalelumi, scaleCMS = self.CMSscale
            PlotHelpers.drawCMSLabel(self.thisCMSLabel, self.canvas.cd(1).GetTopMargin(), self.canvas.cd(1).GetRightMargin(),
                                     scalelumi, scaleCMS, self.moveCMS, self.moveCMSTop, CMSleft = self.CMSLeft,
                                     CMSOneLine = self.CMSOneLine, lumi = self.thisLumi )

        self._ROOTObjects = [
            ratio,
        ]
        if errorband is not None and stacksum is not None:
            self._ROOTObjects.append(ratioErrorband)
        if stacksum is not None:
            self._ROOTObjects.append(stacksum)

        self._ROOTObjectsSet = True
            
        return deepcopy(self.canvas)
                
    def _makeAllRatios(self, stacksum = None, drawPulls = False, groupRatios = False, selfRatio = False):
        if groupRatios:
            logging.warning("Will use the groupRatios feature")
        ListOfRatios = []
        computeRatiosFor = self.histos
        if self.ratioReference is not None:
            logging.info("Prepending new ratiorefernce %s",self.ratioReference)
            computeRatiosFor = [self.ratioReference] + computeRatiosFor
        if self.invertRatioOrder:
            logging.info("Inverting ratio order")
            computeRatiosFor = computeRatiosFor[::-1]
            logging.debug("New order: %s",computeRatiosFor)
        # for h in computeRatiosFor:
        #     print h
        for ihisto, histo in enumerate(computeRatiosFor):
            logging.debug("Make ratio for %s", histo.GetName())
            if ihisto == 0:
                logging.debug("Creating ratio line with histo %s", histo)
                if drawPulls:
                    ListOfRatios.append(self._makeRatioLine(histo, forPulls = True))
                else:
                    ListOfRatios.append(self._makeRatioLine(histo, setRelErr=selfRatio))
            else:
                logging.debug("Creation ratio")
                if stacksum is not None:
                    logging.info("Using stacksum instead of {0} histogram {1}".format(ihisto, histo))
                    if drawPulls:
                        ListOfRatios.append(self._makeRatio(stacksum, computeRatiosFor[0], forPulls = True)[0])
                    else:
                        ListOfRatios.append(self._makeRatio(stacksum, computeRatiosFor[0])[0])
                else:
                    if not groupRatios:
                        if selfRatio:
                            ListOfRatios.append(self._makeRatioSelf(histo, computeRatiosFor[0])[0])
                        else:
                            ListOfRatios.append(self._makeRatio(histo, computeRatiosFor[0], forPulls = drawPulls)[0])
                    else:
                        if ihisto in range(len(self.histos))[::2]: #Skip every other histogram --> will check in list [0, 2, ...]
                            logging.debug("Skipping %s for ratio", ihisto)
                            continue
                        logging.debug("Will calculate ratio for %s w.r.t. %s", ihisto, ihisto-1)
                        ListOfRatios.append(self._makeRatio(histo, self.histos[ihisto-1], forPulls = drawPulls)[0])
        return ListOfRatios

    def _makeRatioLine(self, histo, forPulls = False, setRelErr = False):
        l = histo.Clone("ratioline_"+histo.GetName())
        l.SetTitle("")
        l.Divide(histo)
        l.SetLineColor(histo.GetLineColor())
        l.SetLineStyle(2)
        l.SetLineWidth(1)
        l.SetFillStyle(0)
        lowerbound, upperbound = self.ratioRange
        logging.debug("Ratio range: %s - %s", lowerbound, upperbound)
        l.GetYaxis().SetRangeUser(lowerbound, upperbound)
        l.GetYaxis().SetTitle(self.ratioText)
        l.GetYaxis().SetTitleOffset(1.1)
        l.GetXaxis().SetTitleOffset(0.9)
        l.GetXaxis().SetLabelSize(histo.GetXaxis().GetLabelSize()*(1/0.4))
        l.GetYaxis().SetLabelSize(histo.GetYaxis().GetLabelSize()*(1/0.4))
        l.GetXaxis().SetTitleSize(histo.GetXaxis().GetTitleSize()*(1/0.4))
        l.GetYaxis().SetTitleSize(histo.GetYaxis().GetTitleSize()*(1/0.4))
        l.GetYaxis().SetNdivisions(505)
        l.GetXaxis().SetNdivisions(510)
        #print l.GetXaxis().GetLabelSize()
        for i in range(l.GetNbinsX()+1):
            if not forPulls:
                l.SetBinContent(i,1)
            else:
                l.SetBinContent(i,0)

            
            if setRelErr:
                nomVal = histo.GetBinContent(i)
                errVal = histo.GetBinError(i)
                
                if nomVal != 0:
                    relErr = errVal / float(nomVal)
                else:
                    relErr = 0
                    
                ratioVal = 1
                ratioErrNew = ratioVal*relErr

                logging.debug("Nom = %s | Err = %s | RelErr = %s", nomVal, errVal, relErr)
                logging.debug("ratioVal = %s | ratioErrNew = %s", ratioVal, ratioErrNew)

                
                l.SetBinError(i, ratioErrNew)
                
            else:
                l.SetBinError(i,0)
        logging.debug("Ratio line generated: "+str(l))
        return deepcopy(l)

    def _makeRatio(self, h, href, forPulls = False):
        logging.debug("Making ratio for ratio plot from "+str(h))
        logging.debug("Reference histo: %s", href)
        grref = ROOT.TGraphAsymmErrors(href)
        ratio = grref.Clone("ratio_"+h.GetName())
        ratio.SetMarkerColor(h.GetLineColor())
        ratio.SetLineColor(h.GetLineColor())
        ratio.SetLineStyle(h.GetLineStyle())
        ratio.SetLineWidth(self.ratioLineWidth)
        x, y = ROOT.Double(0), ROOT.Double(0)
        mindiv = 9999.
        maxdiv = -9999.
        for i in range(0,grref.GetN()):
            grref.GetPoint(i, x, y)
            maxErros = max(grref.GetErrorYlow(i), grref.GetErrorYhigh(i))
            currentBin = h.FindBin(x)
            currentBinContent = h.GetBinContent(currentBin)
            currentBinError = h.GetBinError(currentBin)
            ratioErr, ratioval = 0, 1
            if currentBinContent > 0:
                if not forPulls:
                    if y != 0:
                        ratioval = currentBinContent/y if not self.invertRatio else y/currentBinContent
                        ratioErr = (currentBinError/currentBinContent)-(maxErros/y) if not self.invertRatio else (maxErros/y)-(currentBinError/currentBinContent)
                        ratioErr = abs(ratioErr)
                    else:
                        ratioval = 0
                        ratioErr = 0
                else:
                    if maxErros+currentBinError >= 0:
                        if not self.invertRatio:
                            ratioval = (currentBinContent-y)/(maxErros**2+currentBinError**2)**0.5
                        else:
                            ratioval = (y-currentBinContent)/(maxErros**2+currentBinError**2)**0.5
                        logging.debug("Pulls - %s | nh %s | eh %s | nRef %s | eRef %s | Pull %s", i, currentBinContent, currentBinError, y, maxErros,ratioval )
                    else:
                        ratioval = 0
                ratio.SetPoint(i, x, ratioval)
                if ratioval > maxdiv and ratioval > 0:
                    maxdiv = round(ratioval, 1)
                if ratioval < mindiv and ratioval > 0:
                    mindiv = round(ratioval, 1)
            else:
                ratio.SetPoint(i, x, -999)

            if y > 0 and not forPulls:
                if currentBinContent > 0:
                    # ratio.SetPointEYlow(i, grref.GetErrorYlow(i)/currentBinContent)
                    # ratio.SetPointEYhigh(i, grref.GetErrorYhigh(i)/currentBinContent)
                    ratio.SetPointEYlow(i, ratioErr)
                    ratio.SetPointEYhigh(i, ratioErr)
                else:
                    ratio.SetPointEYlow(i, ratioErr)
                    ratio.SetPointEYhigh(i, ratioErr)
                    # ratio.SetPointEYlow(i, 1-(y-grref.GetErrorYlow(i))/y)
                    # ratio.SetPointEYhigh(i, (y+grref.GetErrorYhigh(i))/y-1)
            elif forPulls:
                ratio.SetPointEYlow(i, 1)
                ratio.SetPointEYhigh(i, 1)
            else:
                ratio.SetPointEYlow(i, 0)
                ratio.SetPointEYhigh(i, 0)
            if forPulls:
                ratio.SetPointEXhigh(i, 0)
                ratio.SetPointEXlow(i, 0)
        return [deepcopy(ratio), mindiv, maxdiv]

    def _makeRatioSelf(self, h, href):
        logging.debug("Making the ratio wit relative errors to itsself")
        logging.debug("Making ratio for ratio plot from "+str(h))
        logging.debug("Reference histo: %s", href)
        #grref = ROOT.TGraphAsymmErrors(href)
        #ratio = grref.Clone("ratio_"+h.GetName())
        grh = ROOT.TGraphAsymmErrors(h)
        ratio = grh.Clone("ratio_"+h.GetName())
        hratio = h.Clone("hratio_"+h.GetName())
        ratio.SetMarkerColor(h.GetLineColor())
        ratio.SetLineColor(h.GetLineColor())
        ratio.SetLineWidth(self.ratioLineWidth)

        hratio.Divide(href)
        mindiv = 9999.
        maxdiv = -9999.
        x, y = ROOT.Double(0), ROOT.Double(0)
        for i in range(0,grh.GetN()):
            grh.GetPoint(i, x, y)
            nomVal = h.GetBinContent(i+1)
            errVal = h.GetBinError(i+1)

            if nomVal != 0:
                relErr = errVal / float(nomVal)
            else:
                relErr = 0

            ratioVal = hratio.GetBinContent(i+1)
            ratioErrNew = ratioVal*relErr

            logging.debug("Nom = %s | Err = %s | RelErr = %s", nomVal, errVal, relErr)
            logging.debug("ratioVal = %s | ratioErrNew = %s", ratioVal, ratioErrNew)

            ratio.SetPoint(i, x, ratioVal)
            ratio.SetPointEYlow(i, ratioErrNew)
            ratio.SetPointEYhigh(i, ratioErrNew)
            
            # ratio.SetBinError(i, ratioErrNew)
            if ratioVal > maxdiv and ratioVal > 0:
                maxdiv = round(ratioVal, 1)
            if ratioVal < mindiv and ratioVal > 0:
                mindiv = round(ratioVal, 1)
        
        return [deepcopy(ratio), mindiv, maxdiv]

    
    def getROOTObjects(self):
        if self._ROOTObjectsSet:
            return self.histos +  self._ROOTObjects 
        else:
            return None

    @staticmethod
    def formatErrorband(errorband):
        errorband.SetFillColor(ROOT.kBlack)
        errorband.SetLineColor(ROOT.kBlack)
        errorband.SetFillStyle(3645)
        

def _getRatioErrorBand(stacksum, line):
    ratioErrorBand = line.Clone("ratioErrorBand")
    ratioErrorBand.SetFillColor(ROOT.kBlack)
    ratioErrorBand.SetFillStyle(3645)
    ratioErrorBand.SetMarkerSize(0)
    for ibin in range(stacksum.GetNbinsX()+2):
        error = stacksum.GetBinError(ibin)
        content = stacksum.GetBinContent(ibin)
        errorratio = 0
        if content > 0:
            errorratio = error/float(content)

        logging.debug("Bin %s : error %s", ibin, errorratio)
            
        ratioErrorBand.SetBinError(ibin, errorratio)

    return ratioErrorBand


def _getErrorGraph(histo):
    errorGraph = ROOT.TGraphAsymmErrors(histo)
    errorGraph.SetFillStyle(3645)
    errorGraph.SetFillColor(ROOT.kBlack)
    return errorGraph


