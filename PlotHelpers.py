import ROOT
import copy
import logging
import math
import os
import array

from PLotDrawers import makeCanvasOfHistosBase

def moveOverUnderFlow(histo, moveOverFlow=True, moveUnderFlow=False):
    """
    Function for moving the overflow and (or) underflow bin to the first/last bin
    """
    nBins = histo.GetNbinsX()
    if moveUnderFlow:
        underflow = histo.GetBinContent(0)
        fistBinContent = histo.GetBinContent(1)
        err = math.sqrt(histo.GetBinError(0)*histo.GetBinError(0) + histo.GetBinError(1)*histo.GetBinError(1))
        histo.SetBinContent(1, fistBinContent+underflow)
        histo.SetBinContent(0, 0)
        histo.SetBinError(1,err)
        histo.SetBinError(0,0)
    if moveOverFlow:
        overflow = histo.GetBinContent(nBins+1)
        lastBinContent = histo.GetBinContent(nBins)
        err = math.sqrt(histo.GetBinError(nBins+1)*histo.GetBinError(nBins+1) + histo.GetBinError(nBins)*histo.GetBinError(nBins))
        histo.SetBinContent(nBins, lastBinContent+overflow)
        histo.SetBinContent(nBins+1, 0)
        histo.SetBinError(nBins,err)
        histo.SetBinError(nBins+1,0)
        
def saveCanvasListAsPDF(listofCanvases, outputfilename, foldername):
    logging.info("Writing outputfile %s.pdf",outputfilename)
    if not os.path.isdir(foldername):
        logging.warning("Creating dir %s", foldername)
        os.makedirs(foldername)
    if len(listofCanvases) > 1:
        for icanvas, canves in enumerate(listofCanvases):
            if icanvas == 0:
                canves.Print(foldername+"/"+outputfilename+".pdf[", "pdf")
                #canves.Print(foldername+"/"+outputfilename+".pdf", "pdf")
                canves.Print(foldername+"/"+outputfilename+".pdf", "pdf")
            elif icanvas == len(listofCanvases)-1:
                canves.Print(foldername+"/"+outputfilename+".pdf", "pdf")
                canves.Print(foldername+"/"+outputfilename+".pdf]", "pdf")
            else:
                canves.Print(foldername+"/"+outputfilename+".pdf", "pdf")
    else:
        listofCanvases[0].Print(foldername+"/"+outputfilename+".pdf", "pdf")

def makeDistribution(prefix, histos, Files, selection, mcWeight, color):
    retHistos = []
    for histo, bins, sBin, eBin in histos:
        logging.info("Processing plot: %s",histo)
        h = ROOT.TH1F(histo+"_"+prefix, histo+"_"+prefix, bins, sBin, eBin)
        h.SetTitle("")
        logging.info("Created histos %s", h)
        h.SetLineColor(color)
        for ifile, file_ in enumerate(Files):
            fileName, xsec, ngen = file_
            logging.info("processing file: %s", fileName.GetName().split("/")[-1])
            #logging.info("processing file: %s", fileName.split("/")[-1])
            #rfile = ROOT.TFile.Open(fileName)
            logging.debug(fileName)
            tree = fileName.Get("tree")
            SelTimesWeight = "({0}) * ({1} * {2})".format(selection, 100000 * (xsec/float(ngen)), mcWeight)
            logging.debug("Selection * weight: %s",SelTimesWeight)
            logging.debug("Variable: %s",histo)
            if ifile == 0:
                logging.debug("Projection initial histo %s", h.GetName())
                nProj = tree.Project(h.GetName(), histo, SelTimesWeight)
                logging.debug("Current histo integral: %s", h.Integral())
            else:
                tmpName = h.GetName()+"_tmp"+str(ifile)
                tmph = h.Clone(tmpName)
                nProj = tree.Project(tmpName, histo, SelTimesWeight)
                logging.debug("Current histo integral: %s", tmph.Integral())
                logging.debug("Adding projected histos")
                h.Add(tmph)
            logging.debug("Projection yielded %s events",nProj)
            logging.debug("Total histo integral: %s", h.Integral())
        moveOverUnderFlow(h)
        retHistos.append(h)

    return copy.deepcopy(retHistos)

def drawCMSLabel(addTest = "Preliminary", topMargin = 0.1, rightMargin = 0.1, scaleLumi = 1.0, scaleCMS = 1.0, moveCMS = 0, moveCMSTop = 0, CMSleft = False, CMSOneLine=False, lumi = "41.5", relLumiOffset = 0, extraOffSet=0, hide_lumi = False):
    """
    Drawing the CMS lumi plus logo somewhat tdr stylish

    Args:
    =====
    addTest (str) : The text that is displayed in additon to CMS (stupid typo but don't want to brake to other scripts)
    topMargin, rightMargin (float) : Pass the margin of the canvas you want to logo to be plottet on (0.1 should be ROOT default)
    scaleLumi (float) : Scaling factor for the CMS letters
    moveCMS(float) : Changes the x offset from the axis of the CMS logo part. Negative moves to left, postive moves to right
    moveCMSTop (flaot) : Same as moveCMS but in vertical direction
    CMSleft (bool) : If this is passed, the logo will be plotting in the left side of the plot
    CMSOneLine (bool) : Swithc between one line and two line layouts
    lumi (str) : Value that will be displayed in the lumi label (usually top right corner)
    hide_lumi (bool) : If True is passed, the label with the lumi will not be added to the plot
    relLumiOffset (float) : Additional offset for the lumi label
    extraOffSet (float) : Additional vertical offset for the one-line CMS logo
    """     
    t = topMargin
    r = rightMargin
    cmsText     = "CMS"
    cmsTextFont   = 61  

    logging.debug("Using scale %s, %s",scaleLumi, scaleCMS)
    
    writeExtraText = True
    extraText   = addTest
    extraTextFont = 52 
    
    lumiTextSize     = 0.6 * scaleLumi
    lumiTextOffset   = 0.23 + relLumiOffset
    
    cmsTextSize      = 0.75 * scaleCMS
    cmsTextOffset    = 0.12
    relExtraDY = 0.8

    extraOverCmsTextSize  = 0.76

    extraTextSize = extraOverCmsTextSize*cmsTextSize
    lumiText = lumi+" fb^{-1} (13 TeV)"
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextAngle(0)
    latex.SetTextColor(ROOT.kBlack)

    latex.SetTextFont(42)
    latex.SetTextAlign(31)

    if not hide_lumi:
        latex.SetTextSize(lumiTextSize*t)    

        latex.DrawLatex(1-r,1-t+lumiTextOffset*t,lumiText)

    if CMSleft:
        latex.SetTextAlign(11)
    else:
        latex.SetTextAlign(31)
    
    latex.SetTextFont(cmsTextFont)
    latex.SetTextSize(cmsTextSize*t)
    if CMSleft:
        latex.DrawLatex(0.14+moveCMS, 0.86+moveCMSTop, cmsText)
    else:
        latex.DrawLatex(0.92+moveCMS, 0.86+moveCMSTop, cmsText)
    latex.SetTextFont(extraTextFont)
    latex.SetTextSize(extraTextSize*t)
    if CMSleft:
        if CMSOneLine:
            latex.DrawLatex(0.21+(0.01*(scaleCMS))+moveCMS+extraOffSet, 0.859+moveCMSTop, extraText)
        else:
            latex.DrawLatex(0.14+moveCMS, 0.86+moveCMSTop-relExtraDY*cmsTextSize*t, extraText)
    else:
        latex.DrawLatex(0.92+moveCMS, 0.86+moveCMSTop-relExtraDY*cmsTextSize*t, extraText)
    

    #returns the CMS and Extra textsize
    return cmsTextSize*t, extraTextSize*t
    
def makeCanvasOfHistos(name, Varname, histoList,width=600, height=540, legendText = None,
                       legendSize = (0.5,0.15,0.9,0.4), normalized = False, lineWidth = None,
                       colorList = None, drawAs = "HISTOE", drawAsList = None, addCMS = False,
                       addLabel = None, labelpos = None, legendColumns = 1, topScale = 1.1,
                       addIntLegend = False, supplementary = False, wip = False, lumi = "41.5",
                       yRange = None, showHistoProperties = False, simulation=False):

    if colorList is not None:
        assert isinstance(colorList, list)
        assert len(colorList) == len(histoList)
    
    leg = ROOT.TLegend(legendSize[0], legendSize[1], legendSize[2], legendSize[3])
    leg.SetNColumns(legendColumns)
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)
    leg.SetTextSize(0.03)
    
    integrals = []
    if addIntLegend:
        for histo in histoList:
            integrals.append(histo.Integral())
    
    if legendText is not None:
        assert isinstance(legendText, list)
        assert len(legendText) == len(histoList)
    
    for ihisto, histo in enumerate(histoList):
        if legendText is not None:
            if addIntLegend:
                thisLegText = legendText[ihisto]+" ({0})".format(int(round(integrals[ihisto])))
            else:
                thisLegText = legendText[ihisto]
            leg.AddEntry(histo, thisLegText, "L")
            if showHistoProperties:
                leg.AddEntry(None,
                             "#mu = {:.2f} #sigma = {:.2f}".format(histo.GetMean(), histo.GetStdDev()),
                             "")
            
    canvas = makeCanvasOfHistosBase(name, Varname, histoList, width, height, leg, normalized, lineWidth,
                       colorList, drawAs, drawAsList, addCMS, addLabel, labelpos, topScale, supplementary,
                                    wip, lumi, yRange, showHistoProperties, simulation)
                
    return copy.deepcopy(canvas)
            
def makeCanvas2DwCorrelation(histo, width=1280, height=1000, plotLogZ=False, xstart = 0.27, ystart = 0.95, addIdentifier = "", labelText = None, labelpos = None, addOption = ""):
    return makeCanvas2D(histo, width, height, plotLogZ, xstart, ystart, addIdentifier, labelText, labelpos, True, addOption=addOption)
    
def makeCanvas2D(histo, width=600, height=540, plotLogZ=False, xstart = 0.27, ystart = 0.95, addIdentifier = "", labelText = None, labelpos = None, corrLabel = False, addCMS = False, lumi = "41.5", addOption = "", CMSLabel="Preliminary", topMargin = 0.115, leftMargin = 0.115, rightMargin = 0.16):
    name = histo.GetName()+addIdentifier
    canvas = ROOT.TCanvas("canvas_"+str(name),"canvas_"+str(name),width, height)
    canvas.SetTopMargin(topMargin)
    canvas.SetLeftMargin(leftMargin)
    canvas.SetRightMargin(rightMargin)
    canvas.cd()
    if plotLogZ:
        canvas.SetLogz()
    histo.Draw("colz"+addOption)
    if corrLabel:
        corrL = ROOT.TLatex();
        corrL.SetNDC();
        corrL.SetTextAlign(22);
        corrL.SetTextFont(42);
        corrL.SetTextSizePixels(12);
        corrL.DrawLatex(xstart,ystart,"Correlation: {0:06.4f}".format(histo.GetCorrelationFactor()))
        
    if labelText is not None:
        if labelpos is None:
            xPos, yPos = (0.27, 0.85)
        else:
            xPos, yPos = labelpos
        l = ROOT.TLatex();
        l.SetNDC();
        l.SetTextAlign(13);
        l.SetTextFont(42);
        l.SetTextSizePixels(14);
        l.DrawLatex(xPos, yPos, labelText)
        
    if addCMS:
        thisCMSLabel = CMSLabel
        CMSSize, ExtraSize = drawCMSLabel(thisCMSLabel,
                                          canvas.GetTopMargin(),
                                          canvas.GetRightMargin(),
                                          scaleCMS=0.50,
                                          moveCMSTop = 0.033,
                                          moveCMS = -0.02,
                                          scaleLumi = 0.45,
                                          relLumiOffset = -0.16,
                                          CMSleft = True,
                                          CMSOneLine = True,
                                          lumi = lumi,
                                          extraOffSet=0.006,
        )

    return copy.deepcopy(canvas)

def normalizeHisto(histo):
    if histo.Integral() == 0:
        logging.warning("Histogrma integral 0. Will do nothing")
    else:
        histo.Scale(1/histo.Integral())
        
def initLogging(thisLevel, funcLen = "12"):
    log_format = ('[%(asctime)s] %(funcName)-'+str(funcLen)+'s %(levelname)-8s %(message)s')
    if thisLevel == 20:
        thisLevel = logging.INFO
    elif thisLevel == 10:
        thisLevel = logging.DEBUG
    elif thisLevel == 30:
        thisLevel = logging.WARNING
    elif thisLevel == 40:
        thisLevel = logging.ERROR
    elif thisLevel == 50:
        thisLevel = logging.CRITICAL
    else:
        thisLevel = logging.NOTSET
        
    logging.basicConfig(
        format=log_format,
        level=thisLevel,
        datefmt="%H:%M:%S"
    )


def project(tree, hName, variable, selection, weight, ident = "", onlydebug=False):
    """
    Wrapper for TTree::Project using the logging module
    """
    if onlydebug:
        logging.debug("Projecting %s %s", variable, ident)
    else:
        logging.info("Projecting %s %s", variable, ident)
    logging.debug("Selection: %s", selection)
    logging.debug("   Weight: %s", weight)
    logging.debug("    hName: %s", hName)
    tree.Project(hName, variable, "({0}) * ({1})".format(selection, weight))


def makeStackPlotCanvas(stackName, histos, colors,  legendText, legendSize, width=600, height=540):
    assert len(histos) == len(colors)
    assert len(histos) == len(legendText)

    stack = ROOT.THStack(stackName, stackName)

    leg = ROOT.TLegend(legendSize[0], legendSize[1], legendSize[2], legendSize[3])
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)
    leg.SetTextSize(0.03)
    
    for ihisto, histo in enumerate(histos):
        histo.SetLineColor(ROOT.kBlack)
        histo.SetFillColor(colors[ihisto])
        histo.SetFillStyle(1001)
        stack.Add(histo)
        leg.AddEntry(histo, legendText[ihisto], "F")
        
    name = stackName
    canvas = ROOT.TCanvas("Generalcanvas_"+str(name),"Generalcanvas_"+str(name),width, height)
    canvas.SetTicks(1,1)
    canvas.SetFillStyle(0)
    canvas.SetFillColor(0)

    canvas.SetTopMargin(0.068)
    canvas.SetRightMargin(0.04)
    canvas.SetLeftMargin(0.12)
    canvas.SetBottomMargin(0.12)

    
    stack.Draw("histo")
    leg.Draw("same")

    return copy.deepcopy(canvas)

def checkNcreateFolder(path):
    """
    Helper script for creating folder. If onlyFolder is True
    """
    if not os.path.exists(path):
        logging.warning("Creating direcotries %s", path)
        os.makedirs(path)

def getColorStyleList(nColors):
    """
    Returns a list of colors and line styles that can be used for quick plotting. 
    If more then nColors=len(_colors) is requested linestrlyes will change
    """
    _colors = [ROOT.kBlack,
               ROOT.kBlue, ROOT.kRed, ROOT.kGreen-2,
               ROOT.kCyan, ROOT.kPink, ROOT.kSpring]

    _styles = [1, 9, 10]

    if nColors > len(_colors)*len(_styles):
        raise RuntimeError("nColors exceeding colors * styles. Please add on of the two")


    _totalColors = _colors*len(_styles)
    _totalStyles = []
    for _style in _styles:
        _totalStyles += len(_colors)*[_style]


    colors = []
    styles = []
    for i in range(nColors):
        colors.append(_totalColors[i])
        styles.append(_totalStyles[i])

    return colors, styles


def set_palette(name="default", ncontours=999):
    """Set a color palette from a given RGB list
    stops, red, green and blue should all be lists of the same length
    see set_decent_colors for an example

    Original Author: https://ultrahigh.org/2007/08/making-pretty-root-color-palettes/"""

    if name == "gray" or name == "grayscale":
        stops = [0.00, 0.34, 0.61, 0.84, 1.00]
        red   = [1.00, 0.84, 0.61, 0.34, 0.00]
        green = [1.00, 0.84, 0.61, 0.34, 0.00]
        blue  = [1.00, 0.84, 0.61, 0.34, 0.00]
    # elif name == "whatever":
        # (define more palettes)
    else:
        # default palette, looks cool
        stops = [0.00, 0.34, 0.61, 0.84, 1.00]
        red   = [0.00, 0.00, 0.87, 1.00, 0.51]
        green = [0.00, 0.81, 1.00, 0.20, 0.00]
        blue  = [0.51, 1.00, 0.12, 0.00, 0.00]

    s = array.array('d', stops)
    r = array.array('d', red)
    g = array.array('d', green)
    b = array.array('d', blue)

    npoints = len(s)
    ROOT.TColor.CreateGradientColorTable(npoints, s, r, g, b, ncontours)
    ROOT.gStyle.SetNumberContours(ncontours)


def moveOverflow2D(h):
    for i in range(0,h.GetNbinsX() + 2):
        for j in range(0,h.GetNbinsY() + 2):
            # Skip non overflow or underflow bins
            if i >= 1 and i <= h.GetNbinsX() and j >= 1 and j <= h.GetNbinsY():
                continue
            
            if i == 0 and (j >= 1 and j <= h.GetNbinsY()):
                #print("(%s, %s) is Underflow y (no corner)"%(i,j))
                combX,combY = 1, j
            #Underflow x
            if j == 0 and (i >= 1 and i <= h.GetNbinsX() ):
                #print("(%s, %s) is Underflow x (no corner)"%(i,j))
                combX,combY = i, 1
            #Overflow x
            if i == h.GetNbinsX()+1 and (j >= 1 and j <= h.GetNbinsY()):
                #print("(%s, %s) is Overflow y (no corner)"%(i,j))
                combX,combY = h.GetNbinsX(), j
            #Overflow y
            if j == h.GetNbinsY()+1 and (i >= 1 and i <= h.GetNbinsX() ):
                #print("(%s, %s) is Overflow x (no corner)"%(i,j))
                combX,combY = i, h.GetNbinsY()
            # Lower left corner
            if j == 0 and i == 0:
                #print("(%s, %s) lower left corner"%(i,j))
                combX,combY = 1,1
            # upper left corner
            if j == h.GetNbinsY()+1 and i == 0:
                #print("(%s, %s) upper left corner"%(i,j))
                combX,combY = 1, h.GetNbinsX()
            #upper right corner
            if j == h.GetNbinsY()+1 and i == h.GetNbinsX()+1:
                #print("(%s, %s) upper right corner"%(i,j))
                combX,combY = h.GetNbinsX(), h.GetNbinsY()
            # lower right corner
            if j == 0 and i == h.GetNbinsX()+1:
                #print("(%s, %s) lower right corner"%(i,j))
                combX,combY = h.GetNbinsX(), 1

            #print("COmbining with (%s,%s)"%(combX,combY))

            content = h.GetBinContent(i,j)
            error = h.GetBinError(i,j)

            targetContent = h.GetBinContent(combX,combY)
            targetError = h.GetBinError(combX,combY)

            newContent = content + targetContent
            newError = math.sqrt(error*error + targetError*targetError)

            h.SetBinContent(combX,combY, newContent)
            h.SetBinError(combX,combY, newError)

def plotGraphs(name, listOfGraphs, listOfLabels, xLabel, yLabel, label=None, labelpos=None, width=600, height=540, colors = None, markers =None, legendSize = (0.5,0.15,0.9,0.4), yrange = None, x_range = None, CMSText = "Preliminary", lumi="59.7", vLines = None, yOffset = 1.1, addLeg_entry = None, ret_leg = False , left_margin = 0.12, scaleTitleSize = 1.4):
    """
    Function for creating a canvas with a plot of graphs.

    Args:
    =====
    name (str) : Name of the canvas. Not really important but try to not use the same name for every plot
    listOfGraphs (list) : List of the TGraphs
    listOfLabels (list) : List with strings that will be used in the Legend 
    xLabel (str) : X axis label
    yLabel (str) : Y axis label
    label (str) : Add a text label to the plot. If None is passed, nothing happens; Otherwise pass string. 
    labelpos (tuple) : Position of the label. Tuple of floats. 
    width (int) : Width of the plot, 
    height (int) : Height of the plot 
    colors (list) : Overwrite color for the graphs. If None is passed the default values (see below) will be used 
    markers (list) :  Overwrite markers for the graphs. If None is passed the default values (see below) will be used 
    legendSize (tuple) : Pass a tuple of the four legend postions parameters from the regular constructor 
    yrange (tuple) : Y range of plot - Pass a tuple with (min, max) 
    x_range (tuple) : X range of plot - Pass a tuple with (max, min) --> Makes no real sense. Sorry  
    CMSText (str) : Additional text for the CMS label. Like Preliminary etc. 
    lumi (str) : Lumi value of the top right corner 
    vLines (list) : Pass a list of x positions at which vertical lines will be added to the plot
    yOffset (float) Set the y-axis title offset  
    addLeg_entry (list) : Add additional entries to the legent. The elements of the list should be tuples with (TObj, text, option)
    ret_leg (bool) : If true, the legend will be returned in addition to the canvas 
    left_margin (float) : Set left margin of the plot 
    scaleTitleSize (float) : Set a scaling factor for the x and y title size

    Returns:
    ========
    canvas (ROOT.TCanvas) : Canvas with the plot
    legend (ROOT.TLegend) : Also returns the legend (optional. See ret_leg Arg)
    """
    if colors is None:
        colors = [ROOT.kBlue, ROOT.kRed, ROOT.kGreen+2]

    if markers is None:
        markers = [20, 21, 22]
        
    canvas = ROOT.TCanvas("canvas_"+str(name),"canvas_"+str(name),width, height)
    canvas.SetTopMargin(0.068)
    canvas.SetRightMargin(0.04)
    canvas.SetLeftMargin(left_margin)
    canvas.SetBottomMargin(0.12)

    canvas.cd()

    leg = ROOT.TLegend(legendSize[0], legendSize[1], legendSize[2], legendSize[3])
    leg.SetFillStyle(1001)
    #leg.SetFillColor(ROOT.kOrange)
    leg.SetBorderSize(0)
    leg.SetTextSize(0.04)

    
    for igr, gr in enumerate(listOfGraphs):
        gr.SetTitle("")
        gr.GetXaxis().SetTitle(xLabel)
        gr.GetYaxis().SetTitle(yLabel)

        gr.GetXaxis().SetTitleSize(gr.GetXaxis().GetTitleSize()*scaleTitleSize)
        gr.GetYaxis().SetTitleSize(gr.GetYaxis().GetTitleSize()*scaleTitleSize)

        gr.GetYaxis().SetTitleOffset(yOffset)
        
        gr.SetLineWidth(1)
        gr.SetMarkerSize(1)
        gr.SetMarkerStyle(markers[igr])
        gr.SetMarkerColor(colors[igr])
        gr.SetLineColor(colors[igr])

        drawOptions = "PE"

        if igr == 0:
            gr.Draw("A"+drawOptions)            
            if yrange is not None:
                gr.GetYaxis().SetRangeUser(yrange[1], yrange[0])
            if x_range is not None:
                gr.GetXaxis().SetRangeUser(x_range[1], x_range[0])
            
        gr.Draw(("A" if igr == 0 else "")+drawOptions)
            
        leg.AddEntry(gr, listOfLabels[igr])

    if addLeg_entry is not None:
        for htmp_addLeg, text_addLeg, opt_addLeg in addLeg_entry:
            leg.AddEntry(htmp_addLeg, text_addLeg, opt_addLeg)
        
    leg.Draw("same")

    if CMSText is not None:
        drawCMSLabel(addTest = CMSText,
                     topMargin = canvas.GetTopMargin(),
                     rightMargin = canvas.GetRightMargin(),
                     scaleLumi = 0.85,
                     scaleCMS = 0.85,
                     moveCMS = 0.015,#-0.022,
                     moveCMSTop = 0.022,#0.085 ,
                     CMSleft = True,
                     CMSOneLine= False,#True
                     lumi = lumi)
        
    if vLines is not None :
        if yrange is not None:
            yMax, yMin = yrange
        else:
            yMax = ROOT.gPad.GetUymax()
            yMin = ROOT.gPad.GetUymin()
                    
        allLines = []
        for iline, vLine in enumerate(vLines):
            allLines.append(ROOT.TLine(vLine, yMin, vLine, yMax))
            allLines[iline].SetLineColor(ROOT.kGray+2)
            allLines[iline].SetLineStyle(3)
            allLines[iline].SetLineWidth(2)

            print(yMax, yMin, vLine)
            
        for line in allLines:
            line.Draw("same")


    if label is not None:
        if labelpos is None:
            xPos, yPos = (0.27, 0.85)
        else:
            xPos, yPos = labelpos
        l = ROOT.TLatex();
        l.SetNDC();
        l.SetTextAlign(12);
        l.SetTextFont(42);
        l.SetTextSize(0.04);
        l.DrawLatex(xPos, yPos, label)

    if ret_leg:
        return copy.deepcopy(canvas), leg
    else:
        return copy.deepcopy(canvas)
    
    
    
def getFullColorList():
    baseColors = [
        632,  # kRed
        416,  # kGreen
        600,  # kBlue
        616,  # kMagenta
        432,  # kCyan
        800,  # kOrange
        820,  # kSpring
        840,  # kTeal
        860,  # kAzure
        880,  # kViolet
        900,  # kPink
        400,  # kYellow
    ]

    allColors = []
    for baseColor in baseColors:
        allColors.append(baseColor)
        for i in range(1, 4):
            allColors.append(baseColor+i)
        for i in range(1, 10):
            allColors.append(baseColor-i)

    return allColors
