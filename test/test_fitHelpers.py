import os, sys
sys.path.insert(0, os.path.abspath('..'))

import ROOT
import unittest
import logging
import math

from fitHelpers import getCHiSquare

logger = logging.getLogger()
logger.level = logging.DEBUG
stream_handler = logging.StreamHandler(sys.stdout)
logger.addHandler(stream_handler)

class chi2TestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        stream_handler.stream = sys.stdout
        cls.hUnweighted = ROOT.TH1F("hUnweighted", "hUnweighted", 10, 0, 10)
        cls.hWeighted = ROOT.TH1F("hWeighted", "hWeighted", 10, 0, 10)
        
        for i in range(10):
            cls.hUnweighted.SetBinContent(i+1, (10-i)*10)
            cls.hUnweighted.SetBinError(i+1, math.sqrt((10-i)*10))
            
            if i%2 == 0:
                SF = 1.1
            else:
                SF = 0.9
            cls.hWeighted.SetBinContent(i+1, (10-i)*100*SF)
            cls.hWeighted.SetBinError(i+1, math.sqrt((10-i)*100*SF))
        cls.hWeighted.Scale(0.1)

        for h in [cls.hWeighted,  cls.hUnweighted]:
            logging.info("Histo: %s", h.GetName())
            for i in range(10):
                logging.info("  Bin %s = %s +- %s", i+1, h.GetBinContent(i+1), h.GetBinError(i+1))

        
    def test_normal(self):
        result = getCHiSquare(self.hUnweighted, self.hWeighted)
        expectedChi2Result = self.hUnweighted.Chi2Test(self.hWeighted, "UW")
        self.assertEqual(result[3], False)
        self.assertEqual(result[0], expectedChi2Result)

    def test_h1Zero(self):
        h1Test = self.hUnweighted.Clone("test_h1Zero_hUnweighted")
        h1Test.SetBinContent(10, 0)
        h1Test.SetBinError(10, 0)

        result = getCHiSquare(h1Test, self.hWeighted)

        self.assertEqual(result[3], True)
        
        h2Test = self.hWeighted.Clone("test_h1Zero_hWeighted")
        h2Test.SetBinContent(10, 0)
        h2Test.SetBinError(10, 0)
        
        expectedChi2Result = h1Test.Chi2Test(h2Test, "UW")
        self.assertEqual(result[0], expectedChi2Result)
        
    def test_h2Small(self):
        h2Test = self.hWeighted.Clone("test_h2Small_hWeighted")
        h2Test.SetBinContent(10, 0.2)
        h2Test.SetBinError(10, 1)
        
        result = getCHiSquare(self.hUnweighted, h2Test)

        self.assertEqual(result[3], True)
        
        h1Test = self.hUnweighted.Clone("test_h2Small_hUnweighted")
        h1Test.SetBinContent(10, 0)
        h1Test.SetBinError(10, 0)
        h2Test2 = self.hWeighted.Clone("test2_h2Small_hWeighted")
        h2Test2.SetBinContent(10, 0)
        h2Test2.SetBinError(10, 0)
        expectedChi2Result = h1Test.Chi2Test(h2Test2, "UW")
        self.assertEqual(result[0], expectedChi2Result)
        
if __name__ == '__main__':
    unittest.main()
