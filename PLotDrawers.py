from __future__ import print_function

import logging
import copy
import ROOT

import PlotHelpers# import PlotHelpers.drawCMSLabel, makeCanvasOfHistos


def getFitResultPlot(name, Varname, histo, histolegend, fitFunc, fitLegend,
                     width=600, height=540, listFitValues=True,
                     legendSize = (0.5,0.15,0.9,0.4),
                     histoStyle = None, functionStyle = None,
                     drawCMS=False, Lumi="10.0", CMSText="Work in Progress",
                     replaceParNames = None):
    """
    Args:
      name
      Varname
      histo
      fitFunc
      histolegend
      fitLegend,
      width
      height
      listFitValues
      legendSize 
      histoStyle, FunctionStyle (None or Tuple) : Pass None (default) to use sytle set outside function
                                                  Pass tuple with two elements (ROOT.kCOLOR, line width)
    """
    canvas = ROOT.TCanvas("FitResultscanvas_"+str(name),"FitResultscanvas_"+str(name),width, height)
    canvas.SetTicks(1,1)
    canvas.SetFillStyle(0)
    canvas.SetFillColor(0)

    canvas.SetTopMargin(0.068)
    canvas.SetRightMargin(0.04)
    canvas.SetLeftMargin(0.12)
    canvas.SetBottomMargin(0.12)

    leg = ROOT.TLegend(legendSize[0], legendSize[1], legendSize[2], legendSize[3])
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)
    leg.SetTextSize(0.03)

    canvas.cd()

    if histoStyle is not None:
        try:
            hColor, hWidth = histoStyle
            histo.SetLineColor(hColor)
            histo.SetLineWidth(hWidth)
        except TypeError:
            logging.error("histostyle can not be unpacked. Will use standard settings")
    
    if functionStyle is not None:
        try:
            fColor, fWidth = functionStyle
            fitFunc.SetLineColor(fColor)
            fitFunc.SetLineWidth(fWidth)
        except TypeError:
            logging.error("functionStyle can not be unpacked. Will use standard settings")
    

    histo.SetTitle("")
    histo.GetXaxis().SetTitle(Varname)
    histo.Draw("HISTOE")
    leg.AddEntry(histo, histolegend, "L")

    fitFunc.Draw("SAME")
    leg.AddEntry(fitFunc, fitLegend, "L")
    if listFitValues:
        logging.info("Will list %s paramters", fitFunc.GetNumberFreeParameters())
        useFitFuncNames = True
        if replaceParNames is not None:
            if fitFunc.GetNumberFreeParameters() != len(replaceParNames):
                logging.error("replaceParNames has not the same number of elements as the fitFunc has parameters")
                logging.error("Will keep going")
            else:
                useFitFuncNames = False
        for ipar in range(fitFunc.GetNumberFreeParameters()):
            if useFitFuncNames:
                parName = fitFunc.GetParName(ipar)
            else:
                parName = replaceParNames[ipar]
            leg.AddEntry(None,
                         "{0} = {1:.2f} +- {2:.2f}".format(parName,
                                                           fitFunc.GetParameter(ipar),
                                                           fitFunc.GetParError(ipar)),
                         "")
    leg.Draw("SAME")

    if drawCMS:
        PlotHelpers.drawCMSLabel(addTest = CMSText,
                     topMargin = canvas.GetTopMargin(),
                     rightMargin = canvas.GetRightMargin(),
                     lumi = Lumi)

    canvas.Update()
    return copy.deepcopy(canvas)
    
 
def makeCanvasOfHistosWithLines(name, Varname, histoList, verticalLines, lineColors, horizontalLines = None,
                                width=600, height=540, legendText = None,
                                legendSize = (0.5,0.15,0.9,0.4), normalized = False, lineWidth = None,
                                colorList = None, drawAs = "HISTOE", drawAsList = None, addCMS = False,
                                addLabel = None, labelpos = None, legendColumns = 1, topScale = 1.1,
                                addIntLegend = False, supplementary = False, wip = False, lumi = "41.5",
                                yRange = None, showHistoProperties = False):
    """
    Function runs makeCanvasOfHistos but additionally plots lines.

    Args:
      verticalLines (list) : x positions for vertical lines
      horizontalLines (list) : y positions of horizontal lines
      lineColors (list) : Colors of lines. Is required to be the same len as verticalLines+horizontalLines
                          Colors will first be used for vertical and then horizontal lines.
    """
    nLines = len(verticalLines)
    if horizontalLines is not None:
        nLines += len(horizontalLines)

    if isinstance(lineColors, list):
        assert len(lineColors) == nLines
    else:
        lineColors = nLines*[lineColors]
    
    canvas = PlotHelpers.makeCanvasOfHistos(name, Varname, histoList,  width, height, legendText,
                                            legendSize, normalized, lineWidth, colorList,
                                            drawAs, drawAsList, addCMS, addLabel, labelpos,
                                            legendColumns, topScale, addIntLegend, supplementary,
                                            wip, lumi, yRange, showHistoProperties)

    canvas.cd()

    yMax = ROOT.gPad.GetUymax()
    yMin = ROOT.gPad.GetUymin()
    
    xMax = ROOT.gPad.GetUxmax()
    xMin = ROOT.gPad.GetUxmin()
    
    allLines = []
    for iline, vLine in enumerate(verticalLines):
        allLines.append(ROOT.TLine(vLine, yMin, vLine, yMax))
        allLines[iline].SetLineColor(lineColors[len(allLines)-1])

    if horizontalLines is not None:
        for iline, hLine in enumerate(horizontalLines):
            allLines.append(ROOT.TLine(xMin, hLine, xMax, hLine))
            allLines[iline].SetLineColor(lineColors[len(allLines)-1])

    for line in allLines:
        line.Draw("same")

    return copy.deepcopy(canvas)
    


def makeCanvasOfHistosBase(name, Varname, histoList,width, height, leg, normalized, lineWidth,
                           colorList, drawAs, drawAsList, addCMS, addLabel, labelpos, topScale,
                           supplementary, wip, lumi, yRange, showHistoProperties, simulation = False):
    
    canvas = ROOT.TCanvas("Generalcanvas_"+str(name),"Generalcanvas_"+str(name),width, height)
    canvas.SetTicks(1,1)
    canvas.SetFillStyle(0)
    canvas.SetFillColor(0)

    canvas.SetTopMargin(0.068)
    canvas.SetRightMargin(0.04)
    canvas.SetLeftMargin(0.12)
    canvas.SetBottomMargin(0.12)

    canvas.cd()

    integrals = []
    for histo in histoList:
        integrals.append(histo.Integral())
    
    if normalized:
        for ihisto, histo in enumerate(histoList):
            PlotHelpers.normalizeHisto(histo)

    if drawAsList is None:
        drawAsList = len(histoList)*[drawAs]
        
    maximumVal = 0
    for ihisto, histo in enumerate(histoList):
        if histo.GetBinContent(histo.GetMaximumBin()) > maximumVal:
            maximumVal =  histo.GetBinContent(histo.GetMaximumBin())
            # if maximumVal >= 30:
            #     print histo
    for ihisto, histo in enumerate(histoList):
        histo.SetTitle(" ")
        if colorList is not None:
            histo.SetLineColor(colorList[ihisto])
        histo.GetXaxis().SetTitle(Varname)
        if lineWidth is not None:
            histo.SetLineWidth(lineWidth)
        if maximumVal > 0:
            histo.GetYaxis().SetRangeUser(0,  maximumVal*topScale)

        if yRange is not None:
            logging.debug("Setting range to %s - %s", yRange[0],  yRange[1])
            histo.GetYaxis().SetRangeUser(yRange[0],  yRange[1])
        
        if ihisto == 0:
            #histo.SetTitle(" ")
            if normalized:
                histo.GetYaxis().SetTitle("Normalized Units")
            histo.Draw(drawAsList[ihisto])
        else:
            histo.Draw(drawAsList[ihisto]+"same")
            
    cmssize, ExtraSize = 0.6, 0.75
    if addCMS:
        if supplementary:
            thisCMSLabel = "Supplementary"
        elif wip:
            thisCMSLabel = "Work in Progress"
        elif simulation:
            thisCMSLabel = "Simulation"
        else:
            thisCMSLabel = "Preliminary"
        CMSSize, ExtraSize = PlotHelpers.drawCMSLabel(thisCMSLabel, canvas.GetTopMargin(), canvas.GetRightMargin(), lumi=lumi )
    if addLabel is not None:
        if labelpos is None:
            xPos, yPos = (0.27, 0.85)
        else:
            xPos, yPos = labelpos
        l = ROOT.TLatex();
        l.SetNDC();
        l.SetTextAlign(12);
        l.SetTextFont(42);
        l.SetTextSize(0.04);
        l.DrawLatex(xPos, yPos, addLabel)

    if leg is not None:
        if addCMS:
            leg.SetTextSize(ExtraSize)
        leg.Draw("same")
    canvas.Update()
    return copy.deepcopy(canvas)
    

def makeCanvasOfHistosCustomLeg(name, Varname, histoList, legendObj, width=600, height=540, legendSize = (0.5,0.15,0.9,0.4), normalized = False,
                                lineWidth = None, drawAs = "HISTOE", drawAsList = None, addCMS = False, addLabel = None, colorList=None,
                                labelpos = None, topScale = 1.1, supplementary = False, wip = False, lumi = "41.5", yRange = None,
                                showHistoProperties = False, legendColumns = 1, simulation=False):

    leg = ROOT.TLegend(legendSize[0], legendSize[1], legendSize[2], legendSize[3])
    leg.SetNColumns(legendColumns)
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)
    leg.SetTextSize(0.03)
    
    for obj in legendObj:
        rObj = None
        style = "L"
        thisLegText = ""

        if len(obj) == 2:
            rObj, thisLegText = obj
        elif len(obj) == 3:
            rObj, thisLegText, style = obj
        else:
            raise NotImplementedError("Unsupported number of passed legend objects")
        
        leg.AddEntry(rObj, thisLegText, style)

    canvas = PlotHelpers.makeCanvasOfHistosBase(name, Varname, histoList, width, height, leg, normalized, lineWidth,
                                                colorList, drawAs, drawAsList, addCMS, addLabel, labelpos, topScale, supplementary,
                                                wip, lumi, yRange, showHistoProperties, simulation)
                
    return copy.deepcopy(canvas)
